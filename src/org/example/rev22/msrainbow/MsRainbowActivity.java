/*
 * Copyright (c) 2012,2013 Stefan Völkel <bd@bc-bd.org>
 * Copyright (c) 2014 Michele Bini <michele.bini@gmail.com>
 *
 * Released under the GPL v2. See file License for details.
 *
 * */

package org.example.rev22.msrainbow;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

//For exit-on-swipe
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
// unused: import android.graphics.Point;
import java.lang.Math;
// unused: import android.util.DisplayMetrics;
import android.content.res.Configuration;

public class MsRainbowActivity extends Activity {
	private final static String TAG = "MsRainbowActivity";

	private static final int step = 10;

	View view_;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		view_ = new View(this);
		view_.setKeepScreenOn(true);
		applySettings();
		setContentView(view_);
	}

	@Override
	public void onStart() {
		super.onStart();

		applySettings();
	}

	public void applySettings() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = getBrightness() / 100f;
		getWindow().setAttributes(lp);

		// for full screen mode
		getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);

		if (settings.getBoolean("exit_on_swipe", false)) {
			// for exit-on-swipe
			// swipe_min_distance = settings.getFloat("exit_on_swipe_min_distance", 100.f);
			// swipe_min_speed = settings.getFloat("exit_on_swipe_min_speed", 100.f);
			gestureDetector = new GestureDetector(this, new MyGestureListener(this, settings));
		} else {
			gestureDetector = null;
		}

		flash_on_touch = settings.getBoolean("touch_flash", false);
		rainbow_touch = settings.getBoolean("rainbow_touch", true);

		color = settings.getInt("color", 0xffffffff);
		view_.setBackgroundColor(flash_on_touch ? 0xff000000 : color);	
	}

	private int getBrightness() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		return settings.getInt("brightness", 100);
	}

	private void changeBrightness(int change) {
		int brightness = getBrightness() + change;

		if (brightness > 100)
			brightness = 100;
		if (brightness < step)
			brightness = step;

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("brightness", brightness);
		editor.commit();

		Log.d(TAG, "changed brightness to " + Integer.toString(brightness));
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int action = event.getAction();
		int keyCode = event.getKeyCode();

		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			if (action == KeyEvent.ACTION_UP) {
				changeBrightness(step);
				applySettings();
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			if (action == KeyEvent.ACTION_DOWN) {
				changeBrightness(-step);
				applySettings();
			}
			return true;
		default:
			return super.dispatchKeyEvent(event);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Log.d(TAG, "onPrepareOptionsMenu");
		Intent settingsActivity = new Intent(getBaseContext(),
				Preferences.class);
		startActivity(settingsActivity);
		return true;
	}

    @Override 
    public boolean onTouchEvent(MotionEvent event){
		int action = event.getActionMasked();
    	if (rainbow_touch) {
    		boolean do_process = false;
    		if ((action == MotionEvent.ACTION_DOWN) || (action == MotionEvent.ACTION_POINTER_DOWN)) { 
    			do_process = true;
    			rainbow_touch_id = event.getPointerId(event.getActionIndex());
    			rainbow_touch_ids.add(rainbow_touch_id);
    		} else if (action == MotionEvent.ACTION_POINTER_UP) {
    			do_process = true;
    			rainbow_touch_ids.remove(event.getPointerId(event.getActionIndex()));
    			if (!rainbow_touch_ids.isEmpty()) {
    				rainbow_touch_id = rainbow_touch_ids.iterator().next();
    			}
    		} else if (action == MotionEvent.ACTION_UP) {
    		    rainbow_touch_ids.clear();
    		}
    		if ((do_process || (action == MotionEvent.ACTION_MOVE)) && !rainbow_touch_ids.isEmpty()) {
    			int index = event.findPointerIndex(rainbow_touch_id);
    			if (index >= 0) {
    				float mh = view_.getMeasuredHeight();
    				float mw = view_.getMeasuredWidth();
    				float x = event.getX(index);
    				float y = event.getY(index);
    				float cx = mw * 0.5f;
    				float cy = mh * 0.5f;
	    			float l = ((mh > mw) ? mw : mh)/2;
	    			x -= cx;
	    			y -= cy;
	    			double angle = Math.atan2(x, y);
	    			float d2 = x * x + y * y;
	    			float[] hsb = new float[3];
	    			hsb[1] = (float)Math.min((double)(Math.sqrt(d2)/l), 1.0);
	    			hsb[0] = (float)(((angle / (Math.PI/180.0))+360.0) % 360.0);
	    			hsb[2] = 1.0f;
	    			color = android.graphics.Color.HSVToColor(0xff, hsb);
	    			view_.setBackgroundColor(color);
    			}
    		}
    	}
    	if (flash_on_touch) {
    		if ((action == MotionEvent.ACTION_CANCEL) || (action == MotionEvent.ACTION_UP)) {
    			view_.setBackgroundColor(0xff000000);	
    		} else if (action == MotionEvent.ACTION_DOWN) {
    			view_.setBackgroundColor(color);	
    		}
    	}
    	if (gestureDetector != null) {
    		gestureDetector.onTouchEvent(event);
    	}
        return super.onTouchEvent(event);
    }
	
    // For exit-on-swipe
    // protected float swipe_min_distance = 0.0f;
    // protected float swipe_min_speed = 0.0f;
    protected GestureDetector gestureDetector;
    // protected Object gestureListener;

    protected boolean flash_on_touch;
    protected int color;
    protected boolean rainbow_touch = true;
    protected int rainbow_touch_id;
    protected java.util.Set<Integer> rainbow_touch_ids = new java.util.LinkedHashSet<Integer>();
    
    class MyGestureListener extends SimpleOnGestureListener {
	private MsRainbowActivity mainActivity;
	
	public MyGestureListener(MsRainbowActivity activity, SharedPreferences preferences) {
	    mainActivity = activity;
	}
	
	@Override
        public boolean onDown(MotionEvent event) {
	    // Return true so that gestures can be detected.
            return true;
        }
	
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float vX, float vY) {
	    // float min = mainActivity.swipe_min_speed;
	    
	    // This should work at a later api
	    // Display d = mainActivity.view_.getLayerType()
	    // Point p = new Point(0,0);
	    // d.getSize(p);
	    
	    View v = mainActivity.view_;
	    float mh = v.getMeasuredHeight();
	    float mw = v.getMeasuredWidth();
	    
	    float min_screen_travel = Math.min(mw, mh)/5;
	    // DisplayMetrics metrics = new DisplayMetrics();
	    // d.getMetrics(metrics);
	    float speed_sq = vX*vX + vY*vY;
	    float duration = (event2.getEventTime() - event1.getEventTime())/1000.0f;
	    // estimate screen travel by multiplying swipe speed and duration
	    float length_sq = speed_sq * duration * duration;
	    if (length_sq > min_screen_travel * min_screen_travel) {
		// Swipe speed should be more than about one screen per second
		float minspeed = min_screen_travel*4;
		if (vX*vX + vY*vY > minspeed*minspeed) {
		    mainActivity.finish();
		}
	    }
            return true;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
	// ignore orientation change
	if ((newConfig.orientation != Configuration.ORIENTATION_LANDSCAPE) &&
	    (newConfig.orientation != Configuration.ORIENTATION_PORTRAIT)) {
	    super.onConfigurationChanged(newConfig);
	}
    }
}
